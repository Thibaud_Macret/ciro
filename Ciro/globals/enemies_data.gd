extends Node

#enemies, from txt to array

var enemies_array:Array = []

func _ready():
	load_enemies()

func load_enemies():
	var temp_file = File.new()
	temp_file.open("res://txtdata/enemies.res", File.READ)
	var temp_content = temp_file.get_as_text()
	temp_file.close()
	fill_enemies_array(temp_content)
	create_sub_arrays()

func fill_enemies_array(content):
	var flag_reading:bool = false  #this flag is used to determine if we are in the part we want to read
	var temp_string = ""
	var temp_array = [0,0,0,0,0.0,0.0,0,0,0,0,[],[],[],[]] 
	var temp_enemy = [] #these three will store the data before we put it in enemys_array
	var temp_array_rank:int = 0
#this part is a bit rough : it convert the .txt into an array which we use later
	for content_char in content:
		if(flag_reading):
			if (content_char != ""):
				if (content_char == "|"):
					if(temp_array_rank == 4 || temp_array_rank == 5):
						temp_string = stepify(temp_string.to_float(),0.01) 
					#the fourth and fith data are float, stepify is use because of float inprecision (32 bits and not 64)
					elif(temp_array_rank < 10):
						temp_string = temp_string.to_int() #except for the exceptions, need to convert into int
					temp_array[temp_array_rank] = temp_string
					temp_array_rank += 1
					temp_string = "" #empty temp string
					if (temp_array_rank == 14):
						for index in range (14):
							temp_enemy.push_back(temp_array[index])
						enemies_array.push_back(temp_enemy)
						temp_enemy = []
						temp_array_rank = 0 #store the array corresponding to the enemy and repeat the proccess
						flag_reading = false
				else:
					temp_string = temp_string + content_char #add the char
		
		elif (content_char == "|"): #the first | in each line is used to separate the usefull content from labels
			flag_reading = true

func create_sub_arrays():
	for index in range (enemies_array.size()):
		enemies_array[index][-4] = elem_aray(index)
		for attack_nb in range (3):
			enemies_array[index][-3+attack_nb] = attack_array(index, attack_nb)

func elem_aray(id) -> Array:
	var temp_array:Array = []
	for actual_char in enemies_array[id][-4]:
		if(actual_char == " "):
			return temp_array
		temp_array.push_back(int(actual_char))
	return temp_array

func attack_array(id,attack_number):
	var temp_array:Array = []
	var temp_string:String = ""
	for actual_char in enemies_array[id][-3+attack_number]:
		if(actual_char == "-"): #- means -1
			temp_array.push_back(-1)
		elif(actual_char != " "): #add to the temp number
			temp_string += actual_char
		elif(temp_string != ""): #if there is a space, put the number in the array(if there is a number)
			temp_array.push_back(int(temp_string))
			temp_string = "" #put the number back to 0
	return temp_array
