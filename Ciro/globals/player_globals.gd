extends Node

#All global palyer-related vars

# warning-ignore:unused_class_variable
var potion_stock:int = 5
# warning-ignore:unused_class_variable
var max_life:int = 100
# warning-ignore:unused_class_variable
var actual_life:int = 100
var runes:Array
# warning-ignore:unused_class_variable
var weight:int = 20
# warning-ignore:unused_class_variable
var base_strength:int = 22 #includes the sword
# warning-ignore:unused_class_variable
var base_will:int = 24
# warning-ignore:unused_class_variable
var base_defense:float = 0
# warning-ignore:unused_class_variable
var base_resistance:float = 0
# warning-ignore:unused_class_variable
var base_accuracy:int = 10
# warning-ignore:unused_class_variable
var base_sharpness:int = 20
# warning-ignore:unused_class_variable
var base_element_affinity:Array = [1,1,1,1,1,1,1,1,1,1,4,1,1]
# warning-ignore:unused_class_variable
var level:int = 0
# warning-ignore:unused_class_variable
var next_level_xp:int = 1
var gold:int = 75

func _ready():
	for index in 5: #init all runes 
		runes.append([-1,-1,-1,-1])
	pick_starter_runes()

func pick_starter_runes():
	runes[0][0] = randi() % 8 + 5
	runes[1][0] = randi() % 8 + 5
	if(runes[0][0] == runes[1][0]): #make sure to have two different runes
		runes[1][0] += 1 #no random, just add 1 to the id
		if (runes[1][0] == 13): #if it was the last attack on the list, take the first one
			runes[1][0] = 5

func level_up():
	player_globals.level += 1
	player_globals.next_level_xp = player_globals.level+1 #the total of xp needed to level up is each time 1 more than before
	stats_up()

func stats_up():
	max_life += 10
	actual_life += 10
	if (level > 10):
		max_life += 5
		actual_life += 5
	
	base_strength += 1
	base_will += 2
	base_defense += .01 * (level%2) #def rises at odd levels
	base_resistance += .01 * ((level+1)%2) #res rises at even levels	
	base_accuracy += 1
	base_sharpness += 1
