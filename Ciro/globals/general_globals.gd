extends Node

#Global vars

# warning-ignore:unused_class_variable
var is_combat_timer_running:bool = true

var elements_icon:Array

# warning-ignore:unused_class_variable
onready var comming_enemies_array:Array = []

var map_points:Array = [] #format : [type,xcoord,ycoord], coord are /32
var map_adjacency_matrix:Array = []
# warning-ignore:unused_class_variable
var player_pos:int = 0
# warning-ignore:unused_class_variable
var old_player_pos:int = 0 #used to manage the flight
# warning-ignore:unused_class_variable
var days_left:int = 63

func _ready():
	randomize() #need to be done somewhere
	elements_textures_creation()
	create_map()

func elements_textures_creation():
	for element_id in 13:
		var temptext = load("res://imgs/elements_symbols/" + String(element_id) + ".png")
		elements_icon.append(temptext)

func create_map():
	#temp
	map_points.push_back([0,0,0])
	map_points.push_back([2,1,1])
	map_points.push_back([2,-1,-1])
	map_points.push_back([0,-1,1])
	map_points.push_back([2,1,-1])
	map_points.push_back([1,2,0])
	map_points.push_back([1,0,2])
	map_points.push_back([1,-2,0])
	map_points.push_back([1,0,-2])
	map_points.push_back([1,2,2])
	map_points.push_back([1,-2,2])
	map_points.push_back([1,2,-2])
	map_points.push_back([3,-2,-2])
	map_points.push_back([1,3,1])
	map_points.push_back([1,-3,1])
	map_points.push_back([1,-3,-1])
	map_points.push_back([1,3,-1])
	map_points.push_back([1,1,3])
	map_points.push_back([1,1,-3])
	map_points.push_back([1,-1,-3])
	map_points.push_back([1,-1,3])
	calc_adjacency_matrix()

func calc_adjacency_matrix():
	for point in map_points: #for each point
		var temp_matrix:Array = [] #create it's matrix of adjacency
		for second_point in map_points: #test adjacency with each point
			var dist = abs(point[1]-second_point[1]) + abs(point[2]-second_point[2])
			if (dist == 0 || dist >3): #if the two points are too far away or are the same, they are unconnected
				temp_matrix.push_back(0)
			else:
				temp_matrix.push_back(1)
		map_adjacency_matrix.push_back(temp_matrix)
