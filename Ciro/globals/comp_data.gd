extends Node

#items, from txt to array

var comps_array:Array = []

func _ready():
	load_comps()

func load_comps():
	var temp_file = File.new()
	temp_file.open("res://txtdata/comp.res", File.READ)
	var temp_content = temp_file.get_as_text()
	temp_file.close()
	fill_comps_array(temp_content)

func fill_comps_array(content):
	var flag_reading:bool = false  #this flag is used to determine if we are in the part we want to read
	var temp_string = ""
	var temp_array = [0,0,0,0] 
	var temp_comp = [] #these three will store the data before we put it in enemys_array
	var temp_array_rank:int = 0
#this part is a bit rough : it convert the .txt into an array which we use later
	for content_char in content:
		if(flag_reading):
			if (content_char != ""):
				if (content_char == "|"):
					temp_string = temp_string.to_int() #except for the name, need to convert into int
					temp_array[temp_array_rank] = temp_string
					temp_array_rank += 1
					temp_string = "" #empty temp string
					if (temp_array_rank == 4):
						for index in range (4):
							temp_comp.push_back(temp_array[index])
						comps_array.push_back(temp_comp)
						temp_comp = []
						temp_array_rank = 0 #store the array corresponding to the enemy and repeat the proccess
						flag_reading = false
				else:
					temp_string = temp_string + content_char #add the char
		
		elif (content_char == "|"): #the first | in each line is used to separate the usefull content from labels
			flag_reading = true
