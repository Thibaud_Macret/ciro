extends Node

#player actions in combat content

var actions_array:Array = []
var runes_1a:Array = [] #used for shops : ids of runes type 1a
var runes_1b:Array = [] #...
var runes_2a:Array = []
var runes_3a:Array = []
var runes_4a:Array = []
var runes_4b:Array = []

func _ready():
	load_actions()
	order_runes_for_shops()

func load_actions():
	var temp_file = File.new()
	temp_file.open("res://txtdata/combat_moves.res", File.READ)
	var temp_content = temp_file.get_as_text()
	temp_file.close()
	fill_actions_array(temp_content)

func fill_actions_array(content):
	var flag_reading:bool = false  #this flag is used to determine if we are in the part we want to read
	var temp_string = ""
	var temp_array = ["",0,0,0,0,0,0,0,0,""] 
	var temp_action = [] #these three will store the data before we put it in actions_array
	var temp_array_rank:int = 0
#this part is a bit rough : it convert the .txt into an array which we use later
	for content_char in content:
		if(flag_reading):
			if (content_char != ""):
				if (content_char == "|"):
					if(temp_array_rank == 3 || temp_array_rank == 4):
						temp_string = stepify(temp_string.to_float(),0.1) 
					#the third and fourth data are float, stepify is use because of float inprecision (32 bits and not 64)
					elif(temp_array_rank != 0 && temp_array_rank != 9):
						temp_string = temp_string.to_int() #except for the first data (label), need to convert into int
					temp_array[temp_array_rank] = temp_string
					temp_array_rank += 1
					temp_string = "" #empty temp string
					if (temp_array_rank == 10):
						for index in range (10):
							temp_action.push_back(temp_array[index])
						actions_array.push_back(temp_action)
						temp_action = []
						temp_array_rank = 0 #store the array corresponding to the action and repeat the proccess
						flag_reading = false
				else:
					temp_string = temp_string + content_char #add the char
		
		elif (content_char == "|"): #the first | in each line is used to separate the usefull content from labels
			flag_reading = true

func order_runes_for_shops():
	for index in actions_array.size():
		match actions_array[index][7]:
			1:
				sub_order(actions_array[index], 1).push_back(index)
			2:
				runes_2a.push_back(index)
			3:
				runes_3a.push_back(index)
			4:
				sub_order(actions_array[index], 4).push_back(index) 

func sub_order(action:Array, nature:int) -> Array:
	if(nature == 1):
		if (action[8] == 4):
			return runes_1b
		else:
			return runes_1a
	if(action[8] == 3): #no else because return exit the function
		return runes_4b
	return runes_4a
