Id	Label			Target	Dmg_typ Dmg_mul Special	PV%cost Inertia Nature  Weight  Desc
0 	|Melee			|2  	|1	    |1	    |0	    |0	    |15	    |0      |0      |Inflict *1 to one foe.|
1	|Potion			|1	    |10	    |0.4    |1      |0	    |10	    |0      |0      |Use a potion to *10 40% of max life.|
2	|Guard			|1	    |0	    |0	    |2	    |0  	|5  	|0      |0      |Take reduced damage for one turn.|
3	|Escape 		|1	    |0	    |0	    |3	    |0	    |5	    |0      |0      |Chances to away from the battle.|
4	|Reload 		|1	    |0	    |0	    |4	    |0	    |10	    |0      |0      |Reload all runes.|
5	|Wind Slash		|2	    |2	    |1	    |0	    |1	    |20	    |1      |2      |Inflict medium *02 to one foe.|
6	|Lightning Bolt	|2	    |3  	|1	    |0	    |1	    |20	    |1      |2      |Inflict medium *03 to one foe.|
7	|Wave Crush		|2	    |4	    |1	    |0	    |1  	|20	    |1      |2      |Inflict medium *04 to one foe.|
8	|Icy Slam		|2	    |5	    |1	    |0	    |1  	|20	    |1      |2      |Inflict medium *05 to one foe.|
9	|Fire Spit		|2	    |6	    |1	    |0	    |1  	|20	    |1      |2      |Inflict medium *06 to one foe.|
10	|Wilt	        |2	    |7      |1	    |0	    |1  	|20	    |1      |2      |Inflict medium *07 to one foe.|
11	|Rock Slam		|2	    |8	    |1	    |0	    |1  	|20	    |1      |2      |Inflict medium *08 to one foe.|
12	|Crystal Slash	|2	    |9	    |1	    |0	    |1  	|20	    |1      |2      |Inflict medium *09 to one foe.|
13	|Holy Caress	|2	    |10	    |0.3    |0	    |2  	|25	    |1      |2      |Deal medium *10 to one foe.|
14	|Dark Grasp   	|2	    |11	    |0.3    |0	    |2  	|25	    |1      |2      |Deal medium *11 to one foe.|
15	|Even Strike	|2	    |12	    |1	    |12	    |3  	|25	    |1      |2      |Inflict 2x medium *12 to one foe.|
16	|Gusty Blast	|2	    |2	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *02 to one foe.|
17	|Storm Cry		|2	    |3	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *03 to one foe.|
18	|Sea Havoc		|2	    |4	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *04 to one foe.|
19	|Chilly Rupture |2	    |5	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *05 to one foe.|
20	|Lava coil   	|2	    |6	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *06 to one foe.|
21	|Blight 		|2	    |7	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *07 to one foe.|
22	|Heavy Crush	|2	    |8	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *08 to one foe.|
23	|Crystal Blast  |2	    |9	    |3	    |0	    |25	    |30	    |1      |4      |Inflict severe *09 to one foe.|
24  |Venomous       |0      |0      |0      |31.2   |0      |0      |2      |2      |May cause poison for 2 turns.| 
25  |Revitalizing   |0      |0      |0      |30.3   |0      |0      |2      |2      |Grant regeneration for 3 turns.|
26  |Astonishing    |0      |0      |0      |35.3   |0      |0      |2      |3      |May cause dizzy for 3 turns.|
27  |Iron skin      |0      |0      |0      |34.3   |0      |0      |2      |3      |Grant bulwark for 3 turn.|
28  |Dead weight    |0      |0      |0      |33.2   |0      |0      |2      |2      |May cause weighty for 2 turns.|
29  |LighteLin      |0      |0      |0      |32.3   |0      |0      |2      |2      |Grant light for 3 turns.|
30  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
31  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
32  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
33  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
34  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
35  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
36  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
37  |Strength boost |0      |0      |0      |18.4   |0      |0      |2      |3      |Increase target strength for 4 turns.|
38  |Will boost     |0      |0      |0      |20.4   |0      |0      |2      |3      |Increase target will for 4 turns.|
39  |Strength break |0      |0      |0      |19.3   |0      |0      |2      |3      |Decrease target strength for 3 turns.|
40  |Will break     |0      |0      |0      |21.3   |0      |0      |2      |3      |Decrease target will for 3 turns.|
41  |Defense boost  |0      |0      |0      |22.4   |0      |0      |2      |2      |Increase target defense for 4 turns.|
42  |Res. boost     |0      |0      |0      |24.4   |0      |0      |2      |2      |Increase target resistance for 4 turns.|
43  |Defense break  |0      |0      |0      |23.3   |0      |0      |2      |3      |Decrease target defense for 3 turns.|
44  |Res. break     |0      |0      |0      |25.3   |0      |0      |2      |3      |Decrease target resistance for 3 turns.|
45  |Accuracy boost |0      |0      |0      |26.6   |0      |0      |2      |2      |Increase target accuracy for 6 turns.|
46  |Sharp. boost   |0      |0      |0      |28.5   |0      |0      |2      |2      |Increase target sharpness for 5 turns.|
47  |Accuracy break |0      |0      |0      |27.5   |0      |0      |2      |2      |Decrease target accuracy for 5 turns.|
48  |Sharpness break|0      |0      |0      |29.5   |0      |0      |2      |2      |Decrease target sharpness for 5 turns.|
49  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
50  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
51  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
52  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
53  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
54  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
55  |NONE           |0      |0      |0      |0      |0      |0      |0      |0      |DEL|
56  |Fiery          |0      |0      |0      |36.3   |0      |0      |2      |2      |Grant burning for 3 turns.|
57  |Fiery          |0      |0      |0      |36.4   |0      |0      |2      |2      |Grant burning for 4 turns.|
58  |Fiery          |0      |0      |0      |36.5   |0      |0      |2      |2      |Grant burning for 5 turns.|
59  |Ardent anger   |0      |0      |0      |100    |0      |0      |3      |1      |If you are burning, gain double strike.|
60  |Ardent rage    |0      |0      |0      |101    |0      |0      |3      |3      |If you are burning, gain triple strike.|
61  |Ardent cleanse |0      |0      |0      |102    |0      |0      |3      |1      |If target is burning, remove stats increase.|
62  |Freezing       |0      |0      |0      |37.5   |0      |0      |2      |1      |May cause frost for 5 turns.|
63  |Freezing       |0      |0      |0      |37.6   |0      |0      |2      |1      |May cause frost for 6 turns.|
64  |Freezing       |0      |0      |0      |37.7   |0      |0      |2      |1      |May cause frost for 7 turns.|
65  |Glacial leach  |0      |0      |0      |103    |0      |0      |3      |2      |Consome frost and steal a medium amount of life.|
66  |Glacial crush  |0      |0      |0      |104    |0      |0      |3      |1      |Consome frost and to break strength and will for 5 t.|
67  |Glacial curse  |0      |0      |0      |105    |0      |0      |3      |3      |Consome frost and add 40 to action bar.|
68  |Windy wisp     |0      |2      |0.25   |0      |0      |0      |4      |1      |Deal low *02.|
69  |Lightning wisp |0      |3      |0.25   |0      |0      |0      |4      |1      |Deal low *03.|
70  |Aqua wisp      |0      |4      |0.25   |0      |0      |0      |4      |1      |Deal low *04.|
71  |Icy wisp       |0      |5      |0.25   |0      |0      |0      |4      |1      |Deal low *05.|
72  |Burning wisp   |0      |6      |0.25   |0      |0      |0      |4      |1      |Deal low *06.|
73  |Shattering wisp|0      |7      |0.25   |0      |0      |0      |4      |1      |Deal low *07.|
74  |Rocky wisp     |0      |8      |0.25   |0      |0      |0      |4      |1      |Deal low *08.|
75  |Crystal wisp   |0      |9      |0.25   |0      |0      |0      |4      |1      |Deal low *09.|
76  |Windy pike     |0      |2      |0.5    |0      |0      |0      |4      |3      |Deal small *02.|
77  |Lightning pike |0      |3      |0.5    |0      |0      |0      |4      |3      |Deal small *03.|
78  |Aqua pike      |0      |4      |0.5    |0      |0      |0      |4      |3      |Deal small *04.|
79  |Icy pike       |0      |5      |0.5    |0      |0      |0      |4      |3      |Deal small *05.|
80  |Burning pike   |0      |6      |0.5    |0      |0      |0      |4      |3      |Deal small *06.|
81  |Shattering pike|0      |7      |0.5    |0      |0      |0      |4      |3      |Deal small *07.|
82  |Rocky pike     |0      |8      |0.5    |0      |0      |0      |4      |3      |Deal small *08.|
83  |Crystal pike   |0      |9      |0.5    |0      |0      |0      |4      |3      |Deal small *09.|
84  |Shield bash    |2      |1      |0.25   |2      |0      |10     |1      |1      |Deal low *01 to one foe. Pass in guarding stance.|
85  |Tornado        |3      |2      |1      |0      |7      |30     |1      |2      |Inflict medium *02 to all foes.|
86  |Hurricane      |3      |2      |2      |0      |15     |33     |1      |4      |Inflict high *02 to all foes.|
87  |Tempest        |3      |3      |1      |0      |7      |30     |1      |2      |Inflict medium *03 to all foes.|
88  |Thunderstorm   |3      |3      |2      |0      |15     |33     |1      |4      |Inflict high *03 to all foes.|
89  |Flood          |3      |4      |1      |0      |7      |30     |1      |2      |Inflict medium *04 to all foes.|
90  |Tidal wave     |3      |4      |2      |0      |15     |33     |1      |4      |Inflict high *04 to all foes.|
91  |Hail cloud     |3      |5      |1      |0      |7      |30     |1      |2      |Inflict medium *05 to all foes.|
92  |Frozen mist    |3      |5      |2      |0      |15     |33     |1      |4      |Inflict high *05 to all foes.|
93  |Searing blow   |3      |6      |1      |0      |7      |30     |1      |2      |Inflict medium *06 to all foes.|
94  |Eruption       |3      |6      |2      |0      |15     |33     |1      |4      |Inflict high *06 to all foes.|
95  |Call of decay  |3      |7      |1      |0      |7      |30     |1      |2      |Inflict medium *07 to all foes.|
96  |Withered song  |3      |7      |2      |0      |15     |33     |1      |4      |Inflict high *07 to all foes.|
97  |Rockslide      |3      |8      |1      |0      |7      |30     |1      |2      |Inflict medium *08 to all foes.|
98  |Rocky rain     |3      |8      |2      |0      |15     |33     |1      |4      |Inflict high *08 to all foes.|
99  |Crystal tempest|3      |9      |1      |0      |7      |30     |1      |2      |Inflict medium *09 to all foes.|
100 |Crystal vortex |3      |9      |2      |0      |15     |33     |1      |4      |Inflict high *09 to all foes.|
101 |Mutilation     |1      |1      |0.25   |0      |0      |4      |1      |1      |Inflict low *01 to you.|
102 |Mutilation     |1      |1      |0.25   |0      |0      |4      |1      |1      |Inflict low *01 to you.|
103	|Revitalize		|1	    |10	    |0.55   |1      |0	    |8	    |1      |3      |Use a potion to *10 55% of max life.|
104	|Potion mastery	|1	    |10	    |1      |1      |0	    |6	    |1      |4      |Use a potion to *10 100% of max life.|
105	|Shield mastery	|1	    |10	    |0.1    |2      |0	    |15	    |1      |4      |Pass in guard stance, *10 10% of max life.|
106	|Sword mastery	|3  	|1	    |1	    |0	    |0	    |25	    |0      |2      |Inflict *01 to all foes.|
107	|Holy wipe      |2	    |10	    |0.4    |0	    |10  	|35	    |1      |4      |Deal severe *10 to one foe.|
108	|Dark knock   	|2	    |11	    |0.4    |0	    |10  	|35	    |1      |4      |Deal severe *11 to one foe.|
109	|Equilibrium	|2	    |12	    |3	    |12	    |5  	|40	    |1      |4      |Inflict severe *12 to one foe.|
110 |Attack         |2      |1      |1      |0      |0      |20     |0      |0      |Enemy basic attack.|
111 |Vicious attack |2      |1      |1      |0      |0      |30     |0      |0      |Enemy basic attack variant.|
112 |Fury           |2      |1      |2      |0      |0      |40     |0      |0      |Enemy high attack.|
113 |Preparation    |1      |0      |0      |0      |0      |20     |0      |0      |Enemy self-spell.|
114	|Heal			|4	    |10	    |0.4    |0      |0	    |25	    |0      |0      |Enemy basic heal.|
115 |Handicap       |2      |0      |0      |0      |0      |25     |0      |0      |Enemy basic debuf shell.|
116 |Aid            |3      |0      |0      |0      |0      |35     |0      |0      |Enemy basic all buf shell.|
117 |???			|0	    |0	    |0      |0      |0	    |0	    |0      |0      |Enemy slot|
118 |???			|0	    |0	    |0      |0      |0	    |0	    |0      |0      |Enemy slot|
119 |???			|0	    |0	    |0      |0      |0	    |0	    |0      |0      |Enemy slot|
120 |Quick heal     |1      |10     |0.1    |0      |1      |13     |1      |2      |Deal small *10 to you.|
121 |Quick curse    |1      |11     |0.1    |0      |1      |10     |1      |2      |Deal small *11 to you.|
122 |Typhon         |1      |2      |0.35   |13     |0      |30     |1      |2      |Deal few *2 to you and medium *2 to all foes.|
123 |Call lighting  |1      |3      |0.35   |13     |0      |30     |1      |2      |Deal few *3 to you and medium *3 to all foes.|    
124 |Deluge         |1      |4      |0.35   |13     |0      |30     |1      |2      |Deal few *4 to you and medium *4 to all foes.|    
125 |Biting cold    |1      |5      |0.35   |13     |0      |30     |1      |2      |Deal few *5 to you and medium *5 to all foes.|    
126 |Call inferno   |1      |6      |0.35   |13     |0      |30     |1      |2      |Deal few *6 to you and medium *6 to all foes.|    
127 |Time Distortion|1      |7      |0.35   |13     |0      |30     |1      |2      |Deal few *7 to you and medium *7 to all foes.|    
128 |Meteorite      |1      |8      |0.35   |13     |0      |30     |1      |2      |Deal few *8 to you and medium *8 to all foes.|    
129 |Purple deluge  |1      |9      |0.35   |13     |0      |30     |1      |2      |Deal few *9 to you and medium *9 to all foes.|          
-1  |Empty          |0      |13     |0      |0      |0      |0      |0      |0      | |
Id	Label			Target	Dmg_typ Dmg_mul Special	PV%cost	Inertia Nature  Weight  Desc
    0               1       2       3       4       5       6       7       8       9