extends Area2D

#basic button used to close windows

func _ready():
    # warning-ignore:return_value_discarded
	self.connect("input_event",self,"on_input_event")

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func on_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && event.button_index == 1):
		close_window()

func close_window():
	get_parent().visible = false
	$"/root/Map".is_sec_window_oppended = false
