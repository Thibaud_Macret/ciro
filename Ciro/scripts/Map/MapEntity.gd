extends Area2D

#abstract : basic entity, detect click

func _ready():
    # warning-ignore:return_value_discarded
	self.connect("input_event",self,"on_input_event")

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func on_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && event.button_index == 1):
		if (test_adjacency() && !$"/root/Map".is_sec_window_oppended):
			general_globals.player_pos = get_position_in_parent()
			$"/root/Map/VillageButton".visible = false
			pass_day()
			exec_child_script()

func test_adjacency():
	if(general_globals.map_adjacency_matrix[general_globals.player_pos][get_position_in_parent()]):
		return true
	return false

func pass_day():
	general_globals.days_left -= 1
	$"/root/Map/Timer".actualise()
	$"/root/Map/ExploreBtn".visible = false

func exec_child_script():
	pass

