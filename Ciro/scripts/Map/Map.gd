extends Node2D

#The map view : creates and places entities

var empty_point = load("res://prefabs/Map/EmptyPoint.tscn")
var combat_point = load("res://prefabs/Map/CombatPoint.tscn")
var village_point = load("res://prefabs/Map/VillagePoint.tscn")
var dungeon_point = load("res://prefabs/Map/DungeonPoint.tscn")
# warning-ignore:unused_class_variable
var is_sec_window_oppended:bool = false



func _ready():
	set_visibility()
	place_points()
	place_player()
	test_village()

func set_visibility():
	$"PlayerInfos".visible = false
	$"VillageButton".visible = false
	$"VillageWindow".visible = false

func place_points():
	var nb_points = 0
	for point in general_globals.map_points:
		var point_instance = match_point_instance(point[0])
		$Points.add_child(point_instance)
		$Points.get_child(nb_points).position.x = point[1]*32
		$Points.get_child(nb_points).position.y = point[2]*32
		nb_points+=1

func match_point_instance(type):
	match type:
		0:
			return empty_point.instance()
		1:
			return combat_point.instance()
		2:
			return village_point.instance()
		3:
			return dungeon_point.instance()

func place_player():
	$Player.position.x = $Points.get_child(general_globals.player_pos).position.x
	$Player.position.y = $Points.get_child(general_globals.player_pos).position.y
	$Player.actualise_life_bar()

func test_village(): #need to test if player is in a village after retreat
	if (general_globals.map_points[general_globals.player_pos][0] == 2):
		$"VillageButton".visible = true
		$"/root/Map/VillageWindow/RunesSeller".reload_runes() #avoid the "run bug"
		$"/root/Map/VillageWindow/Training".initiate()




# warning-ignore:unused_argument
func _physics_process(delta):
	place_player()
