extends "res://scripts/Map/MapEmptyPoint.gd"

#Villages

func exec_child_script():
	general_globals.old_player_pos = general_globals.player_pos #moves the player (no need for old_pos)
	$"/root/Map/VillageButton".visible = true #enable the village button
	
	$"/root/Map/VillageWindow/RunesSeller".reload_runes() #pick items wich will be saled
	$"/root/Map/VillageWindow/Training".initiate()
