extends "res://scripts/Button.gd"

#Druid heal button

func triger_click():
# warning-ignore:integer_division
	if (player_globals.gold >= (player_globals.max_life - player_globals.actual_life) / 10):
# warning-ignore:integer_division
		player_globals.gold -= (player_globals.max_life - player_globals.actual_life) / 10
		player_globals.actual_life = player_globals.max_life

# warning-ignore:unused_argument
func actualise_text():
# warning-ignore:integer_division
	if (player_globals.gold >= (player_globals.max_life - player_globals.actual_life +1)/10):
		$Label.add_color_override("font_color",Color(1,1,1))
	else:
		$Label.add_color_override("font_color",Color(1,0,0))
	
	if(player_globals.actual_life == player_globals.max_life):
		$Label.text = "Heal (already full life)"
		$CoinSprite.visible = false
	else:
# warning-ignore:integer_division
		$Label.text = "Heal " + String((player_globals.max_life - player_globals.actual_life)/10)
		$CoinSprite.visible = true
