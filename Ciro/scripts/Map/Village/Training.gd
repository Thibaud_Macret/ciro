extends Node2D

var cost:int = 0

func initiate():
	for index in range (1,3):
		get_child(index).pick_comp()
	cost = 15 + player_globals.level

func reset():
	$"ValidBtn".visible = false
	$"ValidBtn".caller = 0
	$"Comp1/Sprite".frame = 0
	$"Comp2/Sprite".frame = 0
