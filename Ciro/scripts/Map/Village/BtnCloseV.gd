extends "res://scripts/Map/BtnClose.gd"

#only an override on close_window() : need to re-activate the village button

func close_window():
	$"/root/Map".is_sec_window_oppended = false
	$"/root/Map/VillageButton".visible = true
	
	get_parent().visible = false
	$"../Training".reset()
	$"../RunesSeller".unselect_all()