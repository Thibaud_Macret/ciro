extends Node2D

var has_selected_rune:bool = false
var actual_buying_id:int = -1



func on_rune_selected(first_time:bool, type:int, id:int):
	has_selected_rune = first_time
	actual_buying_id = id
	$PlayerRunes.trigger_choosing(first_time, type)



func buy_rune(rune_type:int, rune_index:int): #triggered by childs^3
	player_globals.gold -= get_child(rune_type).price
	$"..".gold_actu()
	player_globals.runes[rune_index-1][rune_type-1] = actual_buying_id
	get_child(rune_type).on_click(false)



func reload_runes():
	for index in range (1,5):
		get_child(index).pick_rune()
		get_child(index).write_text()
	$"RefreshButton".visible = true #will allow one reroll of the runes



func unselect_all():
	for index in range (1,5):
		if (get_child(index).is_selected):
			get_child(index).on_click(false)
