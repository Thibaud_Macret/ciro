extends "res://scripts/Button.gd"

var id:int

func pick_comp():
	id = randi() % comp_data.comps_array.size()
# warning-ignore:integer_division
	actu_text()
	$"../ValidBtn".visible = false

func actu_text():
	$Label.text = match_stat(comp_data.comps_array[id][0]) + write_modif(comp_data.comps_array[id][1])
	if(match_stat(comp_data.comps_array[id][2])):
		 $Label.text += "\n" + match_stat(comp_data.comps_array[id][2]) + write_modif(comp_data.comps_array[id][3])

func match_stat(id:int) ->String :
	match id:
		0:
			return""
		1:
			return "Life"
		2:
			return "Strength"
		3:
			return "Will"
		4:
			return "Defense"
		5:
			return "Resistance"
		6:
			return "Accuracy"
		7:
			return "Sharpness"
		8:
			return "Weight"

func write_modif(modif:int) ->String:
	if (modif < 0):
		return " " + String(modif)
	return " +" + String(modif)



func triger_click():
	if($"../ValidBtn".caller == 0):
		$"../ValidBtn".wait_for_validation(get_position_in_parent())
		$Sprite.frame = 1
		$"../..".unselect_rune()
	elif($"../ValidBtn".caller == get_position_in_parent()):
		$"..".reset()



func buy():
	change_stats()
	$"..".initiate()
	$"..".cost += 15 #after the first train, the cost is increased

func change_stats():
	player_globals.gold -= $"../".cost
	$"../..".gold_actu()
	change_player_stat(comp_data.comps_array[id][0],comp_data.comps_array[id][1])
	change_player_stat(comp_data.comps_array[id][2],comp_data.comps_array[id][3])

func change_player_stat(stat:int, modif:int):
	match stat:
		1:
			player_globals.max_life += modif
			player_globals.actual_life += modif
		2:
			player_globals.base_strength += modif
		3:
			player_globals.base_will += modif
		4:
			player_globals.base_defense += modif*.01
		5:
			player_globals.base_resistance += modif*.01
		6:
			player_globals.base_accuracy += modif
		7:
			player_globals.base_sharpness += modif
		8:
			player_globals.weight += modif
