extends "res://scripts/Button.gd"

var caller:int = 0

func wait_for_validation(caller_pos:int):
# warning-ignore:integer_division
	$Price.text = String($"../".cost)
	self.visible = true
	change_font_color()
	caller = caller_pos

func triger_click():
	if (caller && player_globals.gold >= $"..".cost):
		$"..".get_child(caller).buy()
		$"..".reset()

func change_font_color():
	if (player_globals.gold >= $"..".cost):
		$Price.add_color_override("font_color",Color(1,1,1))
	else:
		$Price.add_color_override("font_color",Color(1,0,0))
