extends Area2D

var can_be_choosen:bool = false
var type:int
var rune_nb:int
var rune:Array
var hovered_time:float = 0



func _ready():
	determine_rank()
	determine_rune()
    # warning-ignore:return_value_discarded
	self.connect("input_event",self,"on_input_event")
    # warning-ignore:return_value_discarded
	self.connect("mouse_entered",self,"mouse_entered")
    # warning-ignore:return_value_discarded
	self.connect("mouse_exited",self,"mouse_exited")

func actualise():
	determine_rank()
	determine_rune()

func determine_rank():
	type = get_position_in_parent()+1
	rune_nb = get_parent().get_position_in_parent()

func determine_rune():
	rune = combat_actions.actions_array[player_globals.runes[rune_nb-1][type-1]]
	if (rune[0] != "Empty"):
		$Effect.frame = rune[2]



func enable_choosing(select:bool):
	can_be_choosen = select
	if(select):
		$Background.frame +=1
	else:
		$Background.frame -= 1



# warning-ignore:unused_argument
# warning-ignore:unused_argument
func on_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && event.button_index == 1):
		if(can_be_choosen):
			$"../../..".buy_rune(type, rune_nb)
			determine_rune()



func _physics_process(delta):
	if (hovered_time):
		hovered_time += delta
		if (hovered_time > .5):
			write_reminder()
			hovered_time = 0

func write_reminder():
	var is_placing_image:bool = false
	var temp_id:String = ""
	$"../Reminder".text = ""
	for info_char in rune[9]:
		if (info_char == "*"): 
			is_placing_image = true #* announces an image
		elif(is_placing_image) :
			if(temp_id == ""): 
				temp_id = info_char #the image id is compsed of 2 chars
			else:
				temp_id += info_char
				$"../Reminder".add_image(general_globals.elements_icon[int(temp_id)])
				is_placing_image = false
				temp_id = ""
		else:
			$"../Reminder".add_text(info_char)



func mouse_entered():
	hovered_time = .01 #serves as a bool and a counter

func mouse_exited():
	hovered_time = 0 #serves as a bool and a counter
	$"../Reminder".clear()

