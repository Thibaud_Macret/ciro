extends Node2D

func trigger_choosing(select:bool, rune_type:int):
	for index in range (1,6):
		get_child(index).get_child(rune_type-1).enable_choosing(select)
