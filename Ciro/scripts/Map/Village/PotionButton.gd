extends "res://scripts/Button.gd"

#Druid sell potions button

func triger_click():
	if (player_globals.gold >= 15 + player_globals.level && player_globals.potion_stock < 10) :
		player_globals.potion_stock += 1
		player_globals.gold -= 15 + player_globals.level
		$"../..".gold_actu()

# warning-ignore:unused_argument
func actualise_text():
	if (player_globals.gold >= 15 + player_globals.level):
		$Label.add_color_override("font_color",Color(1,1,1))
	else:
		$Label.add_color_override("font_color",Color(1,0,0))
		
	if (!player_globals.potion_stock < 10) :
		$Label.text = "Buy potion (MAX)"
		$CoinSprite.visible = false
	else:
		$Label.text = "Buy potion " + String(15 + player_globals.level)
		$CoinSprite.visible = true
