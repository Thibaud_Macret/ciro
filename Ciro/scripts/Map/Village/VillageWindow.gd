extends Node2D

func gold_actu():
	for index in range (1,5):
		$"RunesSeller".get_child(index).actu_price_label()
	$"Druid/PotionButton".actualise_text()
	$"Druid/HealButton".actualise_text()

func unselect_comp():
	$"Training".reset()

func unselect_rune():
	for index in range (1,5):
		if($"RunesSeller".get_child(index).is_selected):
			$"RunesSeller".get_child(index).on_click(false)
