extends "res://scripts/Button.gd"

var is_selected:bool = false
var id:int
var type:int
var effect:String
var price:int



func _ready():
	type = get_position_in_parent() #first rune is always type 1, second is type 2,...
	$Sprite.frame = (type-1)*2
	actu_price_label()

func actu_price_label():
	price = calc_price()
	if(player_globals.gold < price):
		$PriceLbl.add_color_override("font_color",Color(1,0,0))
	else:
		$PriceLbl.add_color_override("font_color",Color(1,1,1))
	$PriceLbl.text = String(price)

func calc_price() ->int:
	match get_position_in_parent():
		1:
			return 20 + player_globals.level
		2:
			return int(12 + player_globals.level*.5)
		3:
			return int(12 + player_globals.level*.5)
		4:
			return int(16 + player_globals.level*.75)



func pick_rune():
	var posible_actions:Array = match_action_array()
	id = (posible_actions[randi() % posible_actions.size()]) #select a random action between all possible
	effect = combat_actions.actions_array[id][9]

func match_action_array() -> Array :
	match type:
		1:
			if(tier2_rune_chance()):
				return combat_actions.runes_1b
			else:
				return combat_actions.runes_1a
		2:
			return combat_actions.runes_2a
		3:
			return combat_actions.runes_3a
		4:
			if(tier2_rune_chance()):
				return combat_actions.runes_4b
			else:
				return combat_actions.runes_4a

func tier2_rune_chance() ->bool:
	var chances:float = 0
	if (player_globals.level > 29): #above level 29, only tier2 rune apear
		return true
	match player_globals.level: #chances increase with level
		10,11,12,13,14:
			chances = .05
		15,16,17,18,19 :
			chances = .20
		20,21,22,23,24 :
			chances = .45
		25,26,27,28,29 :
			chances = .80
	if(randf() < chances):
		return true
	return false



func write_text():
	var is_placing_image:bool = false
	var temp_id:String = ""
	$Text.text = ""
	for info_char in effect:
		if (info_char == "*"): 
			is_placing_image = true #* announces an image
		elif(is_placing_image) :
			if(temp_id == ""): 
				temp_id = info_char #the image id is compsed of 2 chars
			else:
				temp_id += info_char
				$Text.add_image(general_globals.elements_icon[int(temp_id)])
				is_placing_image = false
				temp_id = ""
		else:
			$Text.add_text(info_char)
	if(type == 1):
		$Text.add_text("\n        Inertia : " + String(combat_actions.actions_array[id][6]))



func triger_click():
	if(!get_parent().has_selected_rune && player_globals.gold > price):
		on_click(true)
		$"../..".unselect_comp()
	elif (is_selected):
		on_click(false)

func on_click(select:bool):
	is_selected = select
	get_parent().on_rune_selected(select, type, id)
	change_sprite(select)

func change_sprite(select:bool):
	if(select) : 
		$Sprite.frame += 1
	else : 
		$Sprite.frame -= 1
