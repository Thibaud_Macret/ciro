extends "res://scripts/Map/MapEntity.gd"

#this button can be used when in a village, open the village window

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func on_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && event.button_index == 1):
		if (!$"/root/Map".is_sec_window_oppended):
			exec_script()

func exec_script():
	self.visible = false
	$"../VillageWindow".visible = true
	$"../VillageWindow".gold_actu()
	$"/root/Map".is_sec_window_oppended = true