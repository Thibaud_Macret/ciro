extends "res://scripts/Button.gd"

func _ready():
	set_visibility()

func set_visibility():
	if (player_globals.potion_stock > 0):
		self.visible = true

func triger_click():
	if (player_globals.actual_life < player_globals.max_life):
		player_globals.potion_stock -= 1
		set_visibility()
	# warning-ignore:narrowing_conversion
		player_globals.actual_life += player_globals.max_life * .6
		if (player_globals.actual_life > player_globals.max_life):
			player_globals.actual_life = player_globals.max_life