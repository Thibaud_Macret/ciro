extends Node2D

func _ready():
	actualise()

func actualise():
# warning-ignore:integer_division
	$DaysLeft.text = String(general_globals.days_left)
	$Weeks.text = "Week " + String((63-general_globals.days_left)/7+1)
