extends "res://scripts/Button.gd"

func triger_click():
# warning-ignore:narrowing_conversion
	player_globals.actual_life += player_globals.max_life*0.3
	if(player_globals.actual_life > player_globals.max_life):
		player_globals.actual_life = player_globals.max_life
	pass_day()

func pass_day():
	general_globals.days_left -= 1
	$"/root/Map/Timer".actualise()
	$"/root/Map/VillageWindow/RunesSeller".reload_runes()
	$"/root/Map/VillageWindow/Training".initiate()
