extends "res://scripts/Map/MapEntity.gd"

#contains a combat, launch if player moves

func exec_child_script():
	add_enemy() #first enemy : 100%
	if(randf() < .80): 
		add_enemy() #second enemy = 80%
		if(randf() < .40): 
			add_enemy() #third enemy = 32%
			if(randf() < .30): add_enemy() #fourth enemy = 11%
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Combat_scene.tscn")

func add_enemy():
	general_globals.comming_enemies_array.push_back(enemies_data.enemies_array[randi() %enemies_data.enemies_array.size()])
