extends "res://scripts/Map/MapEntity.gd"

#the player on map view : clicking on it will open the player interface

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func on_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && event.button_index == 1):
		if (!$"/root/Map".is_sec_window_oppended):
			exec_child_script()

func exec_child_script():
	$"/root/Map".is_sec_window_oppended = true
	$"../PlayerInfos".activate()

func actualise_life_bar():
	$"../LifeBar/Text".text = (String(player_globals.actual_life) + " / " + String(player_globals.max_life))
	$"../LifeBar".max_value = player_globals.max_life
	$"../LifeBar".value = player_globals.actual_life
