extends Node2D

#the window wich displays infos about the player

func activate():
	self.visible = true
	actualise_stat_label()
	actualise_xp()
	$PlayerRunes.actualise_all()

func actualise_stat_label():
	$Stats.text = ("Strength : " + String(player_globals.base_strength) + "\nWill : " + String(player_globals.base_will) 
	+ "\nDefense : " + String(player_globals.base_defense*100) + "%\nResistance : " + String(player_globals.base_resistance*100) 
	+ "%\nAccuracy : " + String(player_globals.base_accuracy) + "\nSharpness : " + String(player_globals.base_sharpness)
	+ "\nWeight : " + String(player_globals.weight))

func actualise_xp():
	$ExperienceLbl.text = ("Level : " + String(player_globals.level) + "\n\n\nNext level : " + String(player_globals.next_level_xp))
	$ExperienceBar.max_value = player_globals.level+1
	$ExperienceBar.value = player_globals.level+1 - player_globals.next_level_xp
