extends Button

#button used in combat for action selection

signal click
# warning-ignore:unused_class_variable
var exists:bool = true
export (int) var id
export (int) var id2
export (int) var id3
export (int) var id4

func _ready():
# warning-ignore:return_value_discarded
	self.connect("click", get_parent(), "button_click")
	self.disabled = true
	self.visible = false
	$ProgressBar.visible = false
	text = combat_actions.actions_array[id][0]

func _on_CombatButton_pressed():
	self.emit_signal("click",get_position_in_parent(),id,id2,id3,id4)


func activate():
	self.disabled = false
	self.visible = true
