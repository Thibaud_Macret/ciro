extends Label

#the label displaying damage taken
#text and timer is changed by the parent, this script only manage text disappearance

var timer:int = 0

func _physics_process(delta):
	if(timer>0):
		timer -= delta
		if(timer==0): #in case the value is exactly equal to 0 (few chances but, better be carefull...)
			timer = -1
	elif(timer!=0):
		timer = 0
		self.text = ""