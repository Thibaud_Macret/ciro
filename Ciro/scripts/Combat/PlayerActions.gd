extends Node

#Parent of all CombatButton Nodes, manage the combat actions

signal target

# warning-ignore:unused_class_variable
var rune_uses:Array = [0,0,0,0,0] #each rune coresponds to one number
var actual_action_index:int = 0

func _ready():
# warning-ignore:return_value_discarded
	self.connect("target",get_node("../../"),"change_target_state")
	$Reminder.visible = false

# warning-ignore:unused_argument
func _physics_process(delta):
	for index in 9:
		if(Input.is_action_just_pressed("num"+String(index+1)) && get_child(index).visible): #keybpard shortcut
			get_child(index)._on_CombatButton_pressed()

func able_all():
	for index in range (0,10):
		get_child(index).activate()

func disable_all():
	for index in 10:
		get_child(index).disabled = true
		get_child(index).visible = false

func button_click(button_index,id,id2,id3,id4):
	actual_action_index = button_index
	get_parent().is_active = false
	disable_all()
	get_parent().actual_action_id = id
	get_parent().actual_action_id2 = id2
	get_parent().actual_action_id3 = id3
	get_parent().actual_action_id4 = id4
	manage_reminder_text(id,id2,id3,id4)
	
	self.emit_signal("target",id,true)

func manage_reminder_text(id,id2,id3,id4):
	$Reminder.visible = true
	$Reminder.text = ""
	for actual_id in [id,id2,id3,id4]:
		write_line(actual_id)
		$Reminder.newline()
	
func write_line(id):
	var is_placing_image:bool = false
	var temp_id:String = ""
	for info_char in combat_actions.actions_array[id][9]:
		if (info_char == "*"): 
			is_placing_image = true #* announces an image
		elif(is_placing_image) :
			if(temp_id == ""): 
				temp_id = info_char #the image id is compsed of 2 chars
			else:
				temp_id += info_char
				$Reminder.add_image(general_globals.elements_icon[int(temp_id)])
				is_placing_image = false
				temp_id = ""
		else:
			$Reminder.add_text(info_char)

