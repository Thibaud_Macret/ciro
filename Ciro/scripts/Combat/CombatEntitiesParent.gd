extends Node

#parent of every combat entity, including the label displaying infos

signal damage_taken
signal status_effect_resolve
signal special_effect_resolve

var timer:float = 0

func connect_entities():
	for index in range (1,get_child_count()):
		# warning-ignore:return_value_discarded
		self.connect("damage_taken",get_child(index),"damage_assign")
		# warning-ignore:return_value_discarded
		self.connect("status_effect_resolve",get_child(index),"status_effect_resolve")
		# warning-ignore:return_value_discarded
		self.connect("special_effect_resolve",get_child(index),"special_effect_resolve")

func _physics_process(delta):
	manage_label_timer(delta)
	if(general_globals.is_combat_timer_running):
		manage_waiting_time()
	if(get_child_count() == 2 && $"../Main".current == true): #means that combat is finished because 0 enemy remains
		get_child(1).end_combat(true)

func manage_label_timer(delta:float):
	if (timer>0):
		timer -= delta
		if(timer == 0): #if exactly equal to zero, need to be fixed to -1 in order to trigger bellow
			timer = -1
	if (timer<0):
		timer = 0
		clear_infos()

func clear_infos():
	$Infos.text = ""
	if($Player.waiting_time != -1): #avoid player buf perma reset bug
		general_globals.is_combat_timer_running = true #the combat time is manage by the infos flow

func info_change(text:String): #display text info
	if ($Infos.text == ""):
		$Infos.text = text
	else : #if there is already text, add a new line
		$Infos.text += "\n" + text
	timer = 1.2 #text last for 1,2s


func inflict_damage(target_id:int, damage:float, type:int, zone:bool = false):
	if (!zone || target_id == 1): #if it target the player, zone will not have any effects
		self.emit_signal("damage_taken",target_id,damage, type) 
		#re-send to all entities, the targt_id will permit to determine if the reciving entity is the target
	else :
		for index in (get_child_count()-2) :  #first child is text, second one is self
			self.emit_signal("damage_taken",index+2,damage, type) 

func status_effect_resolve(target_id:int, sender_accuracy:int, status_id:int, status_duration:int, zone:bool = false):
#same as above
	if (!zone || target_id == 1):
		self.emit_signal("status_effect_resolve",target_id, sender_accuracy, status_id, status_duration)
	else :
		for index in (get_child_count()-2) :
			self.emit_signal("status_effect_resolve",index+2 , sender_accuracy, status_id, status_duration)

func special_effect_resolve(target_id:int, special_id:int, caster_position:int, zone:bool = false):
#same as above
	if (!zone || target_id == 1):
		self.emit_signal("special_effect_resolve", target_id, special_id, caster_position)
	else :
		for index in (get_child_count()-2) :
			self.emit_signal("special_effect_resolve", index+2, special_id, caster_position)



func change_target_state(action_id : int, state : bool):
	var target = combat_actions.actions_array[action_id][1]
	if (target == 1):
		get_child(1).is_valid_target = state #1 is self
	else : #1 foe or all foes, either way we need to target all of them
		for index in (get_child_count()-2) :  #first child is text, second one is self
			get_child(index+2).is_valid_target = state



func manage_waiting_time():
	for index in get_child_count()-1:
		get_child(index+1).waiting_time -= 1
	for index in get_child_count()-1:
		if (get_child(index+1).check_turn()):
			index = get_child_count() #break the for
