extends ProgressBar

#a bar wich show when the entity will play. Must have a CombatEntity as parent

var is_previewing_action:bool

# warning-ignore:unused_argument
func _physics_process(delta):
	if(!is_previewing_action):
		var waiting_time:int = int(get_parent().waiting_time)
		self.value = (100 - waiting_time)
		if(waiting_time > 200):
			$Text.text = ""
		elif (waiting_time != -1):
			$Text.text = String(waiting_time)
		else :
			$Text.text = ""

func preview_inertia(time:int):
	is_previewing_action = true
	if(time == $"..".weight):
		$Text.text = ""
	else:
		$Text.text = String(time)
	$Text.add_color_override("font_color",Color(1,0,0))

func cancel_preview():
	is_previewing_action = false
	$Text.add_color_override("font_color",Color(1,1,1))
