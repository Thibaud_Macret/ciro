extends "res://scripts/Combat/CombatButton.gd"

#variant : display life cost if necesary

func _ready():
	$ProgressBar.visible = true
	set_ids()
	if (id == -1): #id = -1 means that this button refers to a rune wich cannot be used
		exists = false
	else:
		set_button_text()

func set_ids():
	var number = get_position_in_parent()
	id = player_globals.runes[number][0]
	id2 = player_globals.runes[number][1]
	id3 = player_globals.runes[number][2]
	id4 = player_globals.runes[number][3]

func set_button_text():
	text = combat_actions.actions_array[id][0]
	if (id != -1 && combat_actions.actions_array[id][5] != 0):
		var temp_cost:int = player_globals.max_life * combat_actions.actions_array[id][5]/100
		text = String(temp_cost) + " : " + text


func activate():
	if(exists):
		self.visible = true
		$ProgressBar.value = 13 - (get_parent().rune_uses[get_position_in_parent()])
		if(get_parent().rune_uses[get_position_in_parent()] <13 && check_potions_consumption() ):
			self.disabled = false

func check_potions_consumption():
	if (player_globals.potion_stock > 0 || !needs_potions()):
		return true
	return false

func needs_potions() ->bool :
	if(combat_actions.actions_array[player_globals.runes[get_position_in_parent()][0]][4] == 1):
		return true
	return false
