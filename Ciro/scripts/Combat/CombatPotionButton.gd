extends "res://scripts/Combat/CombatButton.gd"

#variant : display potion count

func activate():
	self.disabled = false
	self.visible = true
	self.text = ("Potion(" + (convert(player_globals.potion_stock,TYPE_STRING)) +")")
	if (player_globals.potion_stock == 0):
		self.disabled = true
