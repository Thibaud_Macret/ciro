extends Node2D

func enable(experience:int, gold:int):
	$EndCombat.current = true
	if (gold): #golds are only calculated if player won
		xp_and_lvl_up(experience)
		$LootLbl.text = "Experience earned :" + String(experience) + "\nGold looted :" + String(gold)
	else :
		$VictoryLbl.text = "Retreat!"
		$LootLbl.text = ""

func xp_and_lvl_up(total_xp:int):
	var remaining_xp:int = total_xp
	if (remaining_xp < player_globals.next_level_xp): #if don't lvl up
		player_globals.next_level_xp -= remaining_xp
	else :
		remaining_xp -= player_globals.next_level_xp
		player_globals.level_up() #level up, actualise the xp total and then call this func again
		xp_and_lvl_up(remaining_xp)



func _on_Button_pressed():
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Map.tscn")
