extends Node

#base script for combet entities, be it ally or foe

var max_life:int
var base_weight:int
var weight:int
var base_strength:int
var strength:int
var base_will:int
var will:int
var base_defense:float
var defense:float
var base_resistance:float
var resistance:float
var base_accuracy:int
var accuracy:int
var base_sharpness:int
var sharpness:int
var base_element_affinity:Array
var waiting_time:float
var is_active:bool = false
var will_be_stuned:bool = false
var actual_life:int
var element_sprite_timer:float = 0
var is_guarding:bool = false
var is_valid_target:bool = false
var status_effects:Array = []
var element_affinity:Array = [1,1,1,1,1,1,1,1,1,1,4,1,1]
var actual_action_id:int = -1 #-1 means that there's no action in queue
var actual_action_id2:int = -1
var actual_action_id3:int = -1
var actual_action_id4:int = -1
var action:Array = combat_actions.actions_array #just an allias for code visibility
var target_position:int = -1 #-1 means that there's no valid target
# warning-ignore:unused_class_variable
var is_dying:bool = false

func _ready():
	set_attributes_values()
	waiting_time = weight #initial waiting time
	actual_life = max_life

func reset_element_affinity():
	for index in range (13) : 
		element_affinity[index] = base_element_affinity[index]

func set_attributes_values():
	strength = base_strength
	will = base_will
	defense = base_defense
	resistance = base_resistance
	accuracy = base_accuracy
	sharpness = base_sharpness
	weight = base_weight

func _physics_process(delta):
	manage_life_bar()
	manage_weakness_sprite_timer(delta)
	manage_target()

func check_turn() -> bool:
	if(waiting_time <= 0 && actual_action_id == -1):
		if (check_stuns()) : 
			return true
		general_globals.is_combat_timer_running = false
		waiting_time = -1 #set to -1 in order to not loop
		is_active = true #Activate the turn
		return true
	return false

func check_stuns() -> bool:
	if(will_be_stuned):
		return pass_turn("Unabble to act !")
	elif(search_for_status_effect(38) != -1):
		return pass_turn("Frozen !")
	return false

func pass_turn(reason:String) ->bool:
	clear_status()
	$"../".info_change(reason)
	waiting_time = 21 + weight
	return true

func manage_life_bar():
	$LifeBar.max_value = max_life
	$LifeBar.value = actual_life

func check_life(): #inherited
	pass

func execute_action():
	$"../".info_change(action[actual_action_id][0])
	resolve_damages(actual_action_id)
	resolve_special(actual_action_id)
	resolve_id2(actual_action_id2)
	resolve_id3(actual_action_id3, get_position_in_parent())
	resolve_id4(actual_action_id4)

func resolve_damages(id:int) :
	var damage_stat:int = match_damage_stat(id)
	if(damage_stat != -1) :
		if (action[actual_action_id][1] == 3):
			$"../".inflict_damage(target_position, action[id][3]*damage_stat, action[id][2], true)
		else:
			$"../".inflict_damage(target_position,action[id][3]*damage_stat,action[id][2])

func match_damage_stat(id:int) -> int:
	match action[id][2]: #match the element of the attack
		1: #strength depending attack
			return strength
		2,3,4,5,6,7,8,9,12: #will depending attack
			return will
		10, 11: #maxlife attack
			return max_life
# warning-ignore:unreachable_code
	return -1

func resolve_special(id:int):
	match int(action[id][4]):
		1: #cost 1 potion
			player_globals.potion_stock -= 1
		2: #guard
			is_guarding = true
			$GuardIndic.visible = true
		3: #escape (player only)
			if(randf() < 0.5):
				end_combat(false)
			else:
				$"../".info_change("Can't run away !")
		4 : #reload (player only)
			$PlayerActions.rune_uses = [0,0,0,0]
		12: #double strike
			resolve_damages(id)
		13: #repercution (will, x1)
			$"../".inflict_damage(2, will , combat_actions.actions_array[id][2], true)

func resolve_id2(id:int):
	if (id != -1):
		if (action[actual_action_id][1] == 3):
			$"../".status_effect_resolve(target_position, accuracy, int(action[id][4]), 
				( 10*((action[id][4]) - int(action[id][4])) ), true )
		else:
			$"../".status_effect_resolve(target_position, accuracy, int(action[id][4]), 
					( 10*((action[id][4]) - int(action[id][4])) ) )

func resolve_id3(id:int, caster_position:int):
	if (id != -1): #test if there is something to do first
		match int(action[id][4]):
			100: #Ardent anger : If you are burnt, gain double strike
				if (search_for_status_effect(36)!=-1):
					resolve_damages(actual_action_id)
			101: #Ardent rage : If you are burnt, gain triple strike
				if (search_for_status_effect(36)!=-1):
					resolve_damages(actual_action_id)
					resolve_damages(actual_action_id)
			102,103,104,105: #specials wich are effects
				if (action[actual_action_id][1] == 3):
					$"../".special_effect_resolve(target_position, action[id][4], caster_position, true) #aoe
				else:
					$"../".special_effect_resolve(target_position, action[id][4], caster_position) #single target

func resolve_id4(id:int):
	if (id != -1): #test if there is something to do first
		resolve_damages(id)

func finish_and_reset(id:int):
	waiting_time = weight + action[id][6] +1#inertia
	actual_action_id = -1 #reset
	target_position = -1
	get_parent().manage_waiting_time()


func search_for_status_effect(effect_id : int) -> int :
	for index in range (status_effects.size()):
		if (status_effects[index][0] == effect_id):
			return index
	return -1

func count_status_effect(effect_id : int) -> int:
	var count:int = 0
	for index in range (status_effects.size()):
		if (status_effects[index][0] == effect_id):
			count += 1
	return count


func damage_assign(target_id:int, damage:float, type:int):
	if (target_id == get_position_in_parent()): #check if is the target, if so sustain the damages
		var damages:float = apply_armor(damage, type)
		damages = apply_elemental_res(damages, type)
		if(is_guarding):
			damages *= 0.6
		actual_life -= int(damages)
		check_life()
		display_damages(int(damages))
		play_effect(type)

func apply_armor(damage:float, type:int) ->float :
	if (type == 1): #physical attacks
		return(damage * (1-defense))
	elif (type == 10 || type == 11 || type == 0): #life,death and true attacks aren't affected
		return(damage)
	return(damage * (1-resistance)) #will attacks

func apply_elemental_res(damage:float, type:int) ->float :
	criticality_sprite(element_affinity[type])
	match (element_affinity[type]) :
		0:
			return (float(0))
		1:
			return (damage)
		2:
			return (damage * 2.5)
		3:
			return (damage * 0.5)
		4:
			return (-damage)

func criticality_sprite(affinity:int):
	if ($CriticalityIndic.frame == 5): #if first sprite isn't used
		$CriticalityIndic.frame = affinity
	else:
		$CriticalityIndic2.frame = affinity

func display_damages(damages:int):
	if ($lblDamageTaken.text == ""):
		$lblDamageTaken.text = convert(abs(damages), TYPE_STRING)
	else : #is it isn't empty, that means that the attack is a multy-hit
		$lblDamageTaken.text = $lblDamageTaken.text + "   " + convert(abs(damages), TYPE_STRING)
	element_sprite_timer = 0.6

func play_effect(type:int): #TODO
	if(type != 0):
		$Effect/AnimationPlayer.play("Temp_effect")


func status_effect_resolve(target_id:int, sender_accuracy:int, status_id:int, status_duration:int):
	if (target_id == get_position_in_parent()): #check if is the target
		if(status_id < 30): #stats modification
			if(search_for_status_effect(status_id) != -1): #determine if the buff is already applied
				status_effects[search_for_status_effect(status_id)][1] += status_duration
				manage_status_effect_icons()
			else:
				apply_status_effect(status_id,status_duration)
		elif (status_id % 2 == 0): #the even status are sure to be inflicted
			apply_status_effect(status_id,status_duration)
		elif (status_inflict_try(sender_accuracy)): #the odd status aren't sure to be inflicted
			apply_status_effect(status_id,status_duration)

func status_inflict_try(acc:int) ->bool :
	if ( ((acc - sharpness) + 50) > randf()*100): #chance = 50%, plus the diffecrence between accuracy and sharpness
		return true
	return false

func apply_status_effect(status_id:int, status_duration:int):
	status_effects.push_back([status_id,status_duration])
	instant_effects_trigger(status_id) #for some status, effects act imediatly
	manage_status_effect_icons()



func special_effect_resolve(target_id : int, special_id : int, caster_position : int):
	if (target_id == get_position_in_parent()): #check if is the target
		match special_id:
			102:#Ardent cleanse If target is bun, remove all stats buffs
				var has_triggered : bool = false
				if (search_for_status_effect(36) != -1):
					$"../".info_change("Cleanse !")
					for index in range (status_effects.size()-1, -1, -1):
						if (status_effects[index][0] in [18,20,22,24,26,28]):
							status_effects.remove(index)
							has_triggered = true
					if(has_triggered):
						manage_status_effect_icons()
			103: #Glacial leach If target is frost, consome to steal a small amout of HP
				var temp_status_pos : int = search_for_status_effect(37)
				if(temp_status_pos != -1):
					$"../".info_change("Life steal!")
					status_effects.remove(temp_status_pos)
					var caster = get_parent().get_child(caster_position)
					var damages_dealt = actual_life #to know the amout of damages, store life in a temp value
					$"../".inflict_damage(get_position_in_parent(), caster.will, 5)
					damages_dealt -= actual_life
					if (damages_dealt > 0):
						caster.actual_life += damages_dealt
						if (caster.actual_life > caster.max_life) : caster.actual_life = caster.max_life
					manage_status_effect_icons()
			104: #Glacial crush If target is frost, consome to apply strength and will decrease for 5 turns
				var temp_status_pos : int = search_for_status_effect(37)
				if(temp_status_pos != -1):
					$"../".info_change("Crushed !")
					status_effects.remove(temp_status_pos)
					status_effects.append([19,5])
					status_effects.append([21,5])
					manage_status_effect_icons()
			105: #Glacial curse If target is frost consome and add 40 to action bar
				var temp_status_pos : int = search_for_status_effect(37)
				if(temp_status_pos != -1):
					$"../".info_change("Cursed !")
					status_effects.remove(temp_status_pos)
					waiting_time += 40
					manage_status_effect_icons()


func manage_weakness_sprite_timer(delta:float):
	if (element_sprite_timer>0):
		element_sprite_timer -= delta
		if(element_sprite_timer == 0): #if exactly equal to zero, need to be fixed to -1 in order to trigger bellow
			element_sprite_timer = -1
	if (element_sprite_timer<0):
		element_sprite_timer = 0
		$lblDamageTaken.text = ""
		$CriticalityIndic.frame = 5
		$CriticalityIndic2.frame = 5

func manage_target():
	$Combat_target.visible = is_valid_target



func clear_status(): #uses by all child classes : clear all status affects at the begining of the turn
	reset_status()
	reset_element_affinity()
	set_attributes_values()
	manage_status_effects()

func reset_status():
	is_active = false
	is_guarding = false
	$GuardIndic.visible = false
	will_be_stuned = false

func manage_status_effects():
	for index in range(status_effects.size()-1,-1,-1): #check all status effect, starting from the last one (avoid out of range problems)
		turn_effects_trigger(status_effects[index][0], index)
		status_effects[index][1] -= 1
		if (status_effects[index][1] < 1):
			status_effects.remove(index)
		else:
			instant_effects_trigger(status_effects[index][0])
#at the opposite of turn effects, instant effects don't trigger when timer its 0 because they've already triggered one more time when applied
	manage_status_effect_icons()

func instant_effects_trigger(id:int):
	match id:
		22: #defense buff
			if(defense <= base_defense): #non cumulative, so we first check if it is in effect
				defense += .5
		23: #defense debuff
			if(defense >= base_defense):
				defense -= .5
		24: #resistance buff
			if(resistance <= base_resistance):
				resistance += .5
		25: #resistance debuff
			if(resistance >= base_resistance):
				resistance -= .5
		28: #sharpness buff
			if(sharpness <= base_sharpness):
				sharpness += 40
		29: #sharpness debuff
			if(sharpness >= base_sharpness):
				sharpness -= 40
		32: #Light wei -10
			weight -= 10
		33: #Weighty wei +10
			weight +=10
		34: #Bulwark Every defense will be considered as strong (50% reduced damages), except draining ones
			for index in range (element_affinity.size()):
				if(element_affinity[index] < 3):
					element_affinity[index] = 3
		35: #Dissy 35% of chance to miss action
			if (randf() <= 0.35) : 
				will_be_stuned = true
		37: #Frost If 3 stacks, consome to apply frozen 1 (ice)
			if (element_affinity[5] in [1,2]):
				if(count_status_effect(37) == 3):
	# warning-ignore:unused_variable
					for nb_removed in range (3):
						status_effects.remove(search_for_status_effect(37))
					status_effects.append([38,1])
					$"../".info_change("Frozen !")
				manage_status_effect_icons()


func turn_effects_trigger(id:int, index:int):
	match id:
		18: #Strength buff
			if(strength <= base_strength): #non cumulative, so we first check if it is in effect
				strength += base_strength
		19: #Strength debuff
			if(strength >= base_strength):
# warning-ignore:narrowing_conversion
				strength -= base_strength * .5
		20: #Will buff
			if(will <= base_will):
				will += base_will
		21: #Will debuff
			if(will >= base_will):
# warning-ignore:narrowing_conversion
				will -= base_will * .5
		26: #Accuracy buff
			if(accuracy <= base_accuracy):
				accuracy += 40
		27: #Accuracy debuff
			if(accuracy >= base_accuracy):
				accuracy -= 40
		30: #Regeneration : +10% of max. HP per turn
			$"../".inflict_damage(get_position_in_parent(), max_life*.1, 10)
			check_life()
			$"../".info_change("Regeneration !")
		31: #Poison -10% of max. HP per turn
			$"../".inflict_damage(get_position_in_parent(), max_life*.1, 11)
			check_life()
			$"../".info_change("Poison !")
		36: #Burn When fades, -10% of max. HP (fire)
			if(status_effects[index][1] == 1): #if buff is about to fade
				$"../".inflict_damage(get_position_in_parent(), max_life*.1, 6)
				check_life()
				$"../".info_change("Burning !")

func manage_status_effect_icons():
	var last_effect:int = 0
	for index in range(status_effects.size()):
		$Status_infos.get_child(index).region_rect = Rect2(
		(status_effects[index][0]-18)%10*20,(status_effects[index][0]-18)/10*20, 20, 20) #change to coresponding sprite
		$Status_infos.get_child(index).get_child(0).text = String(status_effects[index][1])
		$Status_infos.get_child(index).visible = true
		last_effect = index+1 #place all other icons to unvisible
	for index in range (last_effect,8):
		$Status_infos.get_child(index).visible = false

func end_combat(win:bool):
	$"../..".end_combat(win)
