extends Node2D

#The main node for combat scene

var enemy = load("res://prefabs/Combat/Enemy.tscn")
var total_xp = 0

func _ready():
	reset_vars()
	spawn_enemies()
	calc_xp()
	general_globals.comming_enemies_array.clear()
	create_connections_and_launch()

func reset_vars():
	general_globals.is_combat_timer_running = true

func spawn_enemies():
	for index in (general_globals.comming_enemies_array.size()):
		if (general_globals.comming_enemies_array[index] != []):
			var enemy_instance = enemy.instance()
			$Combat_entities_parent.add_child(enemy_instance) #create the enemy
			$Combat_entities_parent.get_child(index+2).set_attributes(general_globals.comming_enemies_array[index])
			$Combat_entities_parent.get_child(index+2).position.x = -50 + 120*(index+1) #each enemy is placed behind the precedent
			$Combat_entities_parent.get_child(index+2).position.y = 220
		else:
			return #pointless to try next ent if we have an empty one

func create_connections_and_launch():
	$Combat_entities_parent.connect_entities()
	$Combat_entities_parent.manage_waiting_time()

func calc_xp():
	for enemy in (general_globals.comming_enemies_array):
# warning-ignore:integer_division
		total_xp += ((63-general_globals.days_left)/7)+1

func end_combat(win:bool):
	var golds:int
	player_globals.actual_life = $"Combat_entities_parent/Player".actual_life
	change_player_pos(win)
	if(win):
		golds = loot()
	$EndCombatScreen.enable(total_xp, golds)

func change_player_pos(win:bool):
	if(win):
		general_globals.old_player_pos = general_globals.player_pos
		general_globals.map_points[general_globals.player_pos][0] = 0
	else: #run at the last position
		general_globals.player_pos = general_globals.old_player_pos

func loot() ->int:
	var gold:int
	gold = total_xp * (5 + 2*randf()) #gold has a random chance to be increased
	player_globals.gold += gold
	return gold
