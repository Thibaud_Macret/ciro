extends Area2D

#the target wich appear on targets when validating your action, used to know what the player is aiming

func _ready():
# warning-ignore:return_value_discarded
	self.connect("input_event",self,"on_input_event")

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func on_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && event.button_index == 1 && event.button_index == 1):
		$"../../Player".action_confirmed(get_parent().get_position_in_parent())
