extends "res://scripts/Combat/CombatEntity.gd"

#in combat foe script

# warning-ignore:unused_class_variable
var action_1_ids:Array
var action_2_ids:Array
var action_3_ids:Array
var ai_type:int

func set_attributes(attributes:Array):
# warning-ignore:integer_division
	var week = (63 - general_globals.days_left)/7 #the difficulty rises as weeks passes
	max_life = attributes[1] * (week + 1.3) #week + x is here to have difficulty in early game 
	actual_life = max_life
	base_weight = attributes[6]
	base_strength = attributes[2] * (week + 1.3)
	base_will = attributes[3] * (week + 1.3)
	base_defense = attributes[4]
	base_resistance = attributes[5]
	base_accuracy = attributes[7] * (week+1.3)
	base_sharpness = attributes[8] * (week+1.3)
	ai_type = attributes[9]
	base_element_affinity = attributes[-4]
	clear_status()
	
	action_1_ids = attributes[-3]
	action_2_ids = attributes[-2]
	action_3_ids = attributes[-1]
	waiting_time = weight + (15*randf())

	$Sprite.texture = load("res://imgs/enemies/enemy_" + String(attributes[0]) +".png")
	$Sprite/AnimationPlayer.play("Enemy_idle")

func check_life():
	$Sprite/AnimationPlayer.play("Enemy_hurt")
	if(actual_action_id == -1): #if not playing, set back the animation after
		$Sprite/AnimationPlayer.queue("Enemy_idle")
	if (actual_life>max_life):
		actual_life = max_life
	elif (actual_life <= 0):
		$Sprite/AnimationPlayer.play("Enemy_death")
		is_dying = true

func check_death():
	if(is_dying):
		if($Sprite/AnimationPlayer.current_animation != "Enemy_death"):
			queue_free()

# warning-ignore:unused_argument
func _physics_process(delta):
	check_death()
	check_end_turn()
	if (is_active):
		clear_status()
		if (!is_dying):
			take_turn()

func take_turn():
	pick_action()
	change_animation()
	target_position = calc_target_position(actual_action_id)
	execute_action()

func pick_action():
	match (ai_type):
		0: #always action 1
			change_actual_ids(action_1_ids)
		1: #action 2 then change ai type to 0
			change_actual_ids(action_2_ids)
			ai_type = 0
		2: #random between 1 and 2, 3 if <25%HP
			if(actual_life < max_life*.25):
				change_actual_ids(action_3_ids)
			elif(randf()>0.75):
				change_actual_ids(action_2_ids)
			else:
				change_actual_ids(action_1_ids)
		3: #equal chances between actions 1,2 and 3
			if (randf()<.33):
				change_actual_ids(action_1_ids)
			elif (randf()<.5):
				change_actual_ids(action_2_ids)
			else:
				change_actual_ids(action_1_ids)
		4: #life tier determines action
			if(actual_life > max_life*.7):
				change_actual_ids(action_1_ids)
			elif(actual_life > max_life*.3):
				change_actual_ids(action_2_ids)
			else:
				change_actual_ids(action_3_ids)
		5: #2 if has no bufs, 1 overwise
			change_actual_ids(action_2_ids)
			for index in range (status_effects.size()-1, -1, -1):
				if (status_effects[index][0] == int(combat_actions.actions_array[action_2_ids[1]][4])):
					change_actual_ids(action_1_ids)
		6: #healer : dertermins if someone needs heal, else use 2
			target_position = determine_heal_target()
			if(target_position == 0):
				change_actual_ids(action_2_ids)
			else:
				change_actual_ids(action_1_ids)

func determine_heal_target() ->int:
	for index in (get_parent().get_child_count()-2):
		if (get_parent().get_child(index+2).actual_life < get_parent().get_child(index+2).max_life/2):
			return (index+2)
	return 0

func change_animation():
	if(action[actual_action_id][1] == 1):
		$Sprite/AnimationPlayer.play("Enemy_action")
	else:
		$Sprite/AnimationPlayer.play("Enemy_attack")

func change_actual_ids(action:Array):
	actual_action_id = action[0]
	actual_action_id2 = action[1]
	actual_action_id3 = action[2]
	actual_action_id4 = action[3]

func calc_target_position(id:int) ->int :
	match action[id][1]:
		2: #if target player
			return 1
		4: #heal : target is already determined by pick_action
			return target_position
#else : target self or all allies (difference is made in resolve_idx function
# warning-ignore:unreachable_code
	return (get_position_in_parent())

func check_end_turn():
	if($Sprite/AnimationPlayer.current_animation == ""):
		$Sprite/AnimationPlayer.queue("Enemy_idle")
		finish_and_reset(actual_action_id)
