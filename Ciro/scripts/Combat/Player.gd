extends "res://scripts/Combat/CombatEntity.gd"

#in combat player script, always at rank 1 in it's parent

func _ready():
	set_attributes()
	adjust_life_bar()
	$Sprite/AnimationPlayer.play("Player_idle")

func set_attributes():
	max_life = player_globals.max_life
	actual_life = player_globals.actual_life
	base_weight = player_globals.weight
	base_strength = player_globals.base_strength
	base_will = player_globals.base_will
	base_defense = player_globals.base_defense
	base_resistance = player_globals.base_resistance
	base_accuracy = player_globals.base_accuracy
	base_sharpness = player_globals.base_sharpness
	base_element_affinity = player_globals.base_element_affinity

func adjust_life_bar():
# warning-ignore:integer_division
	var temp_size:int = ((max_life/200 +2)*72)
	$LifeBar.rect_size.x = temp_size
	$LifeBar/lblLife.rect_size.x = temp_size



func manage_life_bar(): #overhide, the player has a numeric display
	.manage_life_bar()
	$LifeBar/lblLife.text = convert(actual_life, TYPE_STRING) + "/" + convert(max_life, TYPE_STRING)



func check_life():
	$Sprite/AnimationPlayer.play("Player_hurt")
	$Sprite/AnimationPlayer.queue("Player_idle")
	if (actual_life>max_life):
		actual_life = max_life
	elif (actual_life <= 0):
		$Sprite/AnimationPlayer.play("Player_death")
		is_dying = true



# warning-ignore:unused_argument
func _physics_process(delta):
	check_death()
	check_end_turn(actual_action_id)
	if(actual_action_id != -1): #if is waiting for action validation
		confirm_action()
		$"SpeedBar".preview_inertia(combat_actions.actions_array[actual_action_id][6]+weight)
	elif(is_active):
		clear_status()
		$PlayerActions.able_all()

func check_death():
	if(is_dying):
		if($Sprite/AnimationPlayer.current_animation != "Player_death"):
# warning-ignore:return_value_discarded
			get_tree().change_scene("res://scenes/GameOver.tscn")

func check_end_turn(id:int):
	if($Sprite/AnimationPlayer.current_animation == ""):
		$Sprite/AnimationPlayer.queue("Player_idle")
		$"SpeedBar".cancel_preview()
		.finish_and_reset(id)

func confirm_action():
	if(target_position != -1):
		execute_action()
	elif(Input.is_action_just_pressed("cancel") && $Sprite/AnimationPlayer.current_animation == "Player_idle"):
		$"../".change_target_state(actual_action_id, false)
		$PlayerActions/Reminder.visible = false
		actual_action_id = -1
		$PlayerActions.able_all()

func action_confirmed(target_pos:int):
	target_position = target_pos



func execute_action():
	pay_costs(actual_action_id)
	.execute_action()
	finish_and_reset(actual_action_id)

#in the following functions, id is just an allias for actual_action_id[x]
func pay_costs(id:int):
	if (action[id][5]):
		$"../".inflict_damage(1, action[id][5]*max_life/100,0)
	if ($PlayerActions.actual_action_index < 4): #if using a rune, wear it
		$PlayerActions.rune_uses[$PlayerActions.actual_action_index] += calc_rune_wear()

func calc_rune_wear() ->int :
	var total:int = 0
	total += action[actual_action_id][8]
	if (actual_action_id2!=-1) : total += action[actual_action_id2][8] #2,3,4 aren't always present, so we nedd to test first
	if (actual_action_id3!=-1) : total += action[actual_action_id3][8]
	if (actual_action_id4!=-1) : total += action[actual_action_id4][8]
	return(total)




func finish_and_reset(id:int):
	$"../".change_target_state(id, false)
	if(action[id][1] == 1):
		$Sprite/AnimationPlayer.play("Player_action")
	else:
		$Sprite/AnimationPlayer.play("Player_attack")
	$PlayerActions/Reminder.visible = false
	target_position = -1


